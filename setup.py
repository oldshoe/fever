import os
import sys

from setuptools import find_packages
from setuptools import setup

version = '1.18.0'

install_requires = [
    'setuptools>=39.0.1',
    'jinja2',
    'flask',
    'uwsgi',
]

if not os.environ.get('SNAP_BUILD'):
    install_requires.extend([
        f'acme>={version}',
        f'certbot>={version}',
    ])
elif 'bdist_wheel' in sys.argv[1:]:
    raise RuntimeError('Unset SNAP_BUILD when building wheels '
                       'to include certbot dependencies.')
if os.environ.get('SNAP_BUILD'):
    install_requires.append('packaging')

docs_extras = [
    'Sphinx>=1.0',  # autodoc_member_order = 'bysource', autodoc_default_flags
    'sphinx_rtd_theme',
]

setup(
    name='fever',
    version=version,
    description="AAA server for Freeradius",
    url='https://bitbucket.org/oldshoe/fever',
    author="Leonid Kuzin(aka DgINC)",
    author_email='dg.inc.lcf@gmail.com',
    license='MIT',
    python_requires='>=3.6',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Web Environment',
        'Topic :: Internet :: WWW/HTTP',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Framework :: Flask',
        'Topic :: System :: Networking',
        'Programming Language :: Python :: 3',
    ],

    packages=find_packages(),
    include_package_data=True,
    install_requires=install_requires,
    extras_require={
        'docs': docs_extras,
    },
)