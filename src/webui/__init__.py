# import Flask 
from flask import Flask

# Inject Flask magic
app = Flask(__name__)

# App Config - the minimal footprint
app.config['TESTING'] = True
app.config['SECRET_KEY'] = 'S#perS3crEt_JamesBond'

# Import routing to render the pages
from src.webui import views
